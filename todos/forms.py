from django.forms import ModelForm
from todos.models import TodoList, TodoItem
from django import forms


class TodoListForm(ModelForm):
    name = forms.CharField(max_length=250)

    class Meta:
        model = TodoList
        fields = [
            "name",
        ]
